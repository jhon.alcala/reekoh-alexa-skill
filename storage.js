'use strict';
var AWS = require("aws-sdk");

var storage = (function () {
    var dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

    /*
     * The config class stores all reekoh settings for the user
     */
    function Config(session, data) {
        if (data) {
            this.data = data;
        } else {
            this.data = {
                username: '',
                password: '',
                instance: '',
                dataSet: {}
            };
        }
        this._session = session;
    }

    Config.prototype = {
        save: function (callback) {
            //save the game states in the session,
            //so next time we can save a read from dynamoDB
            this._session.attributes.currentConfig = this.data;
            
            dynamodb.putItem({
                TableName: 'reekohAlexaUserData',
                Item: {
                    userId: {
                        S: this._session.user.userId
                    },
                    Data: {
                        S: JSON.stringify(this.data)
                    }
                }
            }, function (err, data) {
                if (err) {
                    console.log(err, err.stack);
                }
                if (callback) {
                    callback();
                }
            });
        }
    };

    return {
        loadConfig: function (session, callback) {
            if (session.attributes.currentConfig) {
                console.log('get game from session=' + session.attributes.currentConfig);
                callback(new Config(session, session.attributes.currentConfig));
                return;
            }
            
            dynamodb.getItem({
                TableName: 'reekohAlexaUserData',
                Key: {
                    userId: {
                        S: session.user.userId
                    }
                }
            }, function (err, data) {
                var currentConfig;
                if (err) {
                    console.log(err, err.stack);
                    currentConfig = new Config(session);
                    session.attributes.currentConfig = currentConfig.data;
                    callback(currentConfig);
                } else if (data.Item === undefined) {
                    currentConfig = new Config(session);
                    session.attributes.currentConfig = currentConfig.data;
                    callback(currentConfig);
                } else {
                    console.log('get game from dynamodb=' + data.Item.Data.S);
                    currentConfig = new Config(session, JSON.parse(data.Item.Data.S));
                    session.attributes.currentConfig = currentConfig.data;
                    callback(currentConfig);
                }
            });
        },
        newConfig: function (session) {
            return new Config(session);
        }
    };
})();
module.exports = storage;
