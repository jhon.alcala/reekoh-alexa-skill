'use strict';
var AlexaSkill = require('./AlexaSkill'),
    eventHandlers = require('./eventHandlers'),
    intentHandlers = require('./intentHandlers');

var APP_ID = 'amzn1.ask.skill.8e1b0cfe-6a2b-413a-b60a-e30d3acf4214';
var skillContext = {};

var ReekohAlexa = function () {
    AlexaSkill.call(this, APP_ID);
    skillContext.needMoreHelp = true;
};

ReekohAlexa.prototype = Object.create(AlexaSkill.prototype);
ReekohAlexa.prototype.constructor = ReekohAlexa;

eventHandlers.register(ReekohAlexa.prototype.eventHandlers, skillContext);
intentHandlers.register(ReekohAlexa.prototype.intentHandlers, skillContext);

module.exports = ReekohAlexa;