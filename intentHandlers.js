'use strict';

var request = require('request'),
    cheerio = require('cheerio'),
    moment = require('moment'),
    inflect = require('i')(),
    storage = require('./storage'),
    config = require('./config'),
    async = require('async');

var registerIntentHandlers = (intentHandlers, skillContext) => {
    intentHandlers.SetUsernameIntent = (intent, session, response) => {
        storage.loadConfig(session, (currentConfig) => {
            currentConfig.data.username = intent.slots.username.value;

            currentConfig.save(function () {
                response.tell('Username has been set.');
            });
        });
    };

    intentHandlers.SetPasswordIntent = (intent, session, response) => {
        storage.loadConfig(session, (currentConfig) => {
            currentConfig.data.password = intent.slots.password.value;

            currentConfig.save(function () {
                response.tell('Password has been set.');
            });
        });
    };

    intentHandlers.SetInstanceIntent = (intent, session, response) => {
        storage.loadConfig(session, (currentConfig) => {
            currentConfig.data.instance = intent.slots.instance.value.replace(' ', '');

            currentConfig.save(function () {
                response.tell('Instance has been set.');
            });
        });
    };

    intentHandlers.SetRequestDataIntent = (intent, session, response) => {
        storage.loadConfig(session, (currentConfig) => {
            var dataSet = currentConfig.data.dataSet;
            dataSet[intent.slots.data_key.value] = intent.slots.data_value.value;

            currentConfig.data.dataSet = dataSet;

            currentConfig.save(function () {
                response.tell('Request data has been set.');
            });
        });
    };

    intentHandlers.UnsetRequestData = (intent, session, response) => {
        storage.loadConfig(session, (currentConfig) => {
            var dataSet = currentConfig.data.dataSet;
            delete dataSet[intent.slots.data_key.value];

            currentConfig.data.dataSet = dataSet;

            currentConfig.save(function () {
                response.tell(`The request data item ${intent.slots.data_key.value} has been unset.`);
            });
        });
    };

    intentHandlers.AskReekohIntent = (intent, session, response) => {
        let speechOutput = 'Reekoh is an Internet of Things Application Integration Platform for connecting devices, sensors, and data to the best-of-breed cloud and enterprise software or applications.';
        response.tell(speechOutput);
    };

    intentHandlers.ExplainPluginsIntent = (intent, session, response) => {
        let request = require('request');

        request.get('https://reekoh.com/plugin-store/', (error, resp, body) => {
            let totalPlugins = 'plugins vary',
                speechOutput = '';

            if (error) {
                console.log(`error ${error.message}`);
            }
            else if (resp.statusCode !== 200) {
                console.log(`error response ${resp.message}`);
            }
            else {
                let $ = cheerio.load(body, {
                    normalizeWhitespace: true
                });

                totalPlugins = $('#search-plugin #keywords').attr('placeholder');
                totalPlugins = totalPlugins.replace(/\D/g,'');
                totalPlugins = `has a total of ${totalPlugins} plugins varying`;
            }

            speechOutput = `Reekoh plugins provide users the easiest way to integrate I.O.T. data with the largest collection of cloud platforms, tools and services. Reekoh ${totalPlugins} from Connectors, Gateways, Services, Storages, Device Integrations, Exception Handlers, Loggers and Channels.`;
            response.tell(speechOutput);
        });
    };

    intentHandlers.GetLatestPluginsIntent = (intent, session, response) => {
        let speechOutput = 'Apologies. Something went wrong with your request.';

        request.get({
            url: 'https://reekoh.com/plugin-store/?cat=All&pp=1'
        }, (error, resp, body) => {

            if (error) {
                console.log(`error ${error.message}`);
            }
            else if (resp.statusCode !== 200) {
                console.log(`error response ${resp.message}`);
            }
            else {
                let $ = cheerio.load(body, {
                    normalizeWhitespace: true
                });

                let totalPlugins = $('#search-plugin #keywords').attr('placeholder');
                totalPlugins = totalPlugins.replace(/\D/g,'');

                let plugins = $('.plugin-section .plugin-link').slice(0,5).map(function(i, elem) {
                    return $(this).text();
                }).get();

                let last_plugin = plugins.pop();
                plugins = plugins.join(', ') + ` and ${last_plugin}`;
                
                speechOutput = `Reekoh currently has a total of ${totalPlugins} plugins. Their latest realeases are, ${plugins}`;
            }
            
            response.tell(speechOutput);
        });
    };

    intentHandlers.GetLatestBlogPostIntent = (intent, session, response) => {
        let speechOutput = 'Apologies. Something went wrong with your request.';

        request.get({
            url: 'https://reekoh.com/blog/'
        }, (error, resp, body) => {

            if (error) {
                console.log(`error ${error.message}`);
            }
            else if (resp.statusCode !== 200) {
                console.log(`error response ${resp.message}`);
            }
            else {
                let $ = cheerio.load(body, {
                        normalizeWhitespace: true
                    }),
                    curYear = moment().format('YYYY');
                
                let blogPosts = $('.article').slice(0,5).map(function(i, article) {

                    let datePosted = $('.media-meta', article).text(),
                        articleTitle = $('.media-heading', article).text();

                    datePosted = datePosted.split('|').shift().trim();
                    datePosted = moment(datePosted, 'MMMM DD, YYYY');

                    if(datePosted.format('YYYY') == curYear) {
                        datePosted = datePosted.format('MMMM D');
                    }
                    else {
                        datePosted = datePosted.format('MMMM D, YYYY');
                    }

                    return `${articleTitle}, posted ${datePosted}.`;
                }).get();

                let lastPost = blogPosts.pop();
                speechOutput = `The latest reekoh blog posts are, ${blogPosts.join(' ')} And ${lastPost}. Check our website at www.reekoh.com, for more news and updates`;
            }
            
            response.tell(speechOutput);
        });
    };

    intentHandlers.GetLatestTweetsIntent = (intent, session, response) => {
        var tweets = [], speechOutput = 'Apologies. Something went wrong with your request.', dates = [];

        request.get({
            url: 'https://twitter.com/reekohiot'
        }, (error, resp, body) => {
            if (error) {
                console.log(`error ${error.message}`);
            }
            else if (resp.statusCode !== 200) {
                console.log(`error response ${resp.message}`);
            }
            else{
                var $ = cheerio.load(body);

                tweets = $('.TweetTextSize').slice(0,5).map(function(i, el) {
                    var temp = $(el).text().replace(/(?:https?):\/\/[\n\S]+/g, '');
                    temp = temp.replace(/(?:pic.twitter.com?)\/[\n\S]+/g, '');
                    temp = temp.replace('#', '');
                    temp = temp.replace('IoT', 'I.O.T.');
                    temp = temp.replace('PaaS', 'Platform as a Service');

                    return temp.trim();
                }).get();

                dates = $('.tweet-timestamp').slice(0,5).map(function(i, el) {
                    return moment($(this).attr('title'), 'h:mm A - MMM D, YYYY').format('MMM D, h:mm A');
                }).get();

                speechOutput = '';

                for( let c = 0; c < 5; c++){
                    speechOutput += `${dates[c]} - ${tweets[c]}.`;
                }
                response.tell('Here are the latest tweets by Reekoh: ' + speechOutput + ' Follow us @Reekoh I.O.T.');
            }
        });
    };

    intentHandlers.ActivateSprinklerIntent = (intent, session, response) => {
        let speechOutput = 'Greenhouse sprinkler has been activated.';
        response.tell(speechOutput);
    };

    intentHandlers.SendData = (intent, session, response) => {
        var sessionConfig;
        async.waterfall([
            (next) => {
                if(session.attributes.currentConfig){
                    sessionConfig = session.attributes.currentConfig;
                    next();
                }
                else{
                    storage.loadConfig(session, (currentConfig) => {
                        sessionConfig = currentConfig.data;
                        next();
                    });
                }
            },
            (next) => {
                if(!sessionConfig.username){
                    next('Username is not set. Please set your username first.');
                }
                else if(!sessionConfig.password){
                    next('Password is not set. Please set your password first.');
                }
                else if(!sessionConfig.instance){
                    next('Instance is not set. Please set your instance first.');
                }
                else
                    next();
            }
        ], (error) => {
            if(error){
                response.tell(error);
                return;
            }

            let reekohInstance = `http://${sessionConfig.instance}.reekoh.com:${config.port}/reekoh/data`;

            sessionConfig.dataSet.device = config.device;

            request.post({
                url: reekohInstance,
                json: sessionConfig.dataSet
            }, (error, resp, body) => {
                let speechOutput = '';

                if (error) {
                    console.log(`error ${JSON.stringify(error)}`);
                    speechOutput = error.message;
                }
                else if (resp.statusCode !== 200) {
                    console.log(`error response ${JSON.stringify(resp)}`);
                    speechOutput = resp.statusMessage;
                }

                if(speechOutput)
                    speechOutput = `Something went wrong with your request. Respsonse message was : ${speechOutput}`;
                else
                    speechOutput = `Data was successfuly sent to reekoh.`;

                return response.tell(speechOutput);
            });
        });
    };

    intentHandlers['AMAZON.HelpIntent'] = (intent, session, response) => {
        response.ask('Here are some things you can say. ask riko support to define itself. ask riko support about their plugins. ask riko support latest blog posts. ask riko support tweets. What do you want to do?', 'What do you want to do?');
    };

    intentHandlers['AMAZON.CancelIntent'] = (intent, session, response) => {
        response.tell('Alright. You can give your command whenever you\'re ready.');
    };

    intentHandlers['AMAZON.StopIntent'] = (intent, session, response) => {
        response.tell('Alright. You can give your command whenever you\'re ready.');
    };
};

exports.register = registerIntentHandlers;