'use strict';
var ReekohAlexa = require('./reekohAlexa');

exports.handler = function (event, context) {
    var reekohAlexa = new ReekohAlexa();
    reekohAlexa.execute(event, context);
};