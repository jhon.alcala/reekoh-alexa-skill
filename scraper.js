'use strict';
var request = require('request'),
    cheerio = require('cheerio'),
    moment = require('moment');

request.get('https://reekoh.com/plugin-store/', (error, resp, body) => {

    let speechOutput = 'Apologies. Something went wrong with your request.',
    	cheerio = require('cheerio');

    // request.get({
    //     url: 'https://plugins.reekoh.com/searchplugins?limit=0&sort=-created_dt',
    //     json: true
    // }, (error, resp, body) => {
            
    //     let firstFivePlugins = body.slice(0, 5);
    //     console.log(body)
    //     console.log(firstFivePlugins)
    //     // firstFivePlugins = firstFivePlugins.map((plugin) => {
    //     //     return firstFivePlugins.name
    //     // });

    //     // let last_plugin = firstFivePlugins.pop();
    //     // firstFivePlugins = firstFivePlugins.join(', ') + ` and ${last_plugin}`;

    //     // speechOutput = `Reekoh currently has a total of ${body.length} plugins. Their latest realease are, ${firstFivePlugins}`;
    //     // console.log(speechOutput);
    // });

    // request.get({
    //     url: 'https://reekoh.com/plugin-store/?cat=All&pp=1'
    // }, (error, resp, body) => {
    //     let $ = cheerio.load(body, {
    //         normalizeWhitespace: true
    //     });

    //     let totalPlugins = $('#search-plugin #keywords').attr('placeholder');
    //     totalPlugins = totalPlugins.replace(/\D/g,'');

    //     let plugins = $('.plugin-section .plugin-link').slice(0,5).map(function(i, elem) {
    //         return $(this).text();
    //     }).get();

    //     let last_plugin = plugins.pop();
    //     plugins = plugins.join(', ') + ` and ${last_plugin}`;
        
    //     speechOutput = `Reekoh currently has a total of ${totalPlugins} plugins. Their latest realeases are, ${plugins}`;
    //     console.log(speechOutput);
    // });







    // request.get({
    //     url: 'https://reekoh.com/blog/'
    // }, (error, resp, body) => {

    //     if (error) {
    //         console.log(`error ${error.message}`);
    //     }
    //     else if (resp.statusCode !== 200) {
    //         console.log(`error response ${resp.message}`);
    //     }
    //     else {
    //         let $ = cheerio.load(body, {
    //                 normalizeWhitespace: true
    //             }),
    //             curYear = moment().format('YYYY');
            
    //         let blogPosts = $('.article').slice(0,5).map(function(i, article) {

    //             let datePosted = $('.media-meta', article).text(),
    //                 articleTitle = $('.media-heading', article).text();

    //             datePosted = datePosted.split('|').shift().trim();
    //             datePosted = moment(datePosted, 'MMMM DD, YYYY');

    //             if(datePosted.format('YYYY') == curYear) {
    //                 datePosted = datePosted.format('MMMM D');
    //             }
    //             else {
    //                 datePosted = datePosted.format('MMMM D, YYYY');
    //             }

    //             return `${articleTitle}, posted ${datePosted}.`;
    //         }).get();

    //         let lastPost = blogPosts.pop();
    //         speechOutput = `The latest reekoh blog posts are, ${blogPosts.join(' ')} And ${lastPost}`;
    //     }
        
    //     console.log(speechOutput);
    // });

    request.get({
        url: 'https://twitter.com/reekohiot'
    }, (error, resp, body) => {
         if (error) {
            console.log(`error ${error.message}`);
        }
        else if (resp.statusCode !== 200) {
            console.log(`error response ${resp.message}`);
        }
        else {
            let $ = cheerio.load(body, {
                normalizeWhitespace: true
            });

            let tweets = $('.js-tweet-text-container').slice(0,5).map(function(i, el) {
                    return $(this).text().trim();
                }).get();

            
            tweets = tweets.join('.').replace(/(?:https?):\/\/[\n\S]+/g, '').replace(/(?:pic.twitter.com?)\/[\n\S]+/g, '').replace('#', '');
            speechOutput = `Here are the lastest tweets from Reekoh. ${tweets}`;
        }

        console.log(speechOutput);
    });
});